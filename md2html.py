#-*- coding: utf-8 -*-

import markdown
import sys

def main():
    textfile = open(sys.argv[1])
    text = textfile.read()
    textfile.close()
    text = text.decode(encoding='UTF-8')
    text = markdown.markdown(text,extensions=['markdown.extensions.toc','markdown.extensions.fenced_code','markdown.extensions.extra'])
    text = text.encode(encoding='UTF-8')
    print(text)

if __name__ == '__main__':
    main()
